package utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import model.Schedule
import serialization.SchedDeserial
import serialization.SchedNewTimeDeserial
import serialization.SchedSerial

object JacksonUtils{
    val mapper : ObjectMapper = ObjectMapper().registerModule(JavaTimeModule())!!

    init {
        val module = SimpleModule()
        module.addSerializer(SchedSerial())
        module.addDeserializer(Schedule::class.java, SchedDeserial())
        mapper.registerModule(module)
    }
    init {
        val module = SimpleModule()
        module.addSerializer(SchedSerial())
        module.addDeserializer(Schedule::class.java, SchedNewTimeDeserial())
        mapper.registerModule(module)
    }
}