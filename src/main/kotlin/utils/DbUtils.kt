package utils

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import model.Schedule
import model.User
import utils.Constants.DB_PATH
import java.time.format.DateTimeFormatter

object DbUtils {

    val source : ConnectionSource = JdbcConnectionSource(DB_PATH)
    val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
//    var date = LocalDate.parse("31-12-2018", formatter) example of formatter utilization

    init {
        TableUtils.createTableIfNotExists(source, Schedule::class.java)
        TableUtils.createTableIfNotExists(source, User::class.java)
    }
}