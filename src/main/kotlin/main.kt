import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import controller.ScheduleController
import io.javalin.Context
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.json.JavalinJackson
import io.javalin.security.Role
import io.javalin.security.SecurityUtil.roles
import model.enums.MyRole
import model.User
import org.mindrot.jbcrypt.BCrypt
import utils.Constants
import utils.DbUtils
import utils.JacksonUtils

fun main() {
    val app = Javalin.create()
        .enableDebugLogging()
        .port(Constants.PORT)

    JavalinJackson.configure(JacksonUtils.mapper)

    app.accessManager { handler, ctx, permittedRoles ->
        val role = getUserRole(ctx)
        if (permittedRoles.contains(role)){
            handler.handle(ctx)
        } else ctx.status(401).result("Unauthorized")
    }

    app.routes{
        get("", { ctx -> ctx.result("Try to make any request")}, roles(MyRole.ADMIN, MyRole.TEACHER, MyRole.STUDENT))
        get("/schedule", { ctx -> ScheduleController.getAll(ctx)}, roles(MyRole.ADMIN, MyRole.TEACHER, MyRole.STUDENT))
        get("/schedule:id", { ctx -> ScheduleController.getOne(ctx, ctx.pathParam("id"))}, roles(MyRole.ADMIN, MyRole.TEACHER, MyRole.STUDENT))
        post("/schedule", { ctx -> ScheduleController.create(ctx)}, roles(MyRole.ADMIN, MyRole.TEACHER))
        patch ("/schedule:id", { ctx -> ScheduleController.update(ctx, ctx.pathParam("id"))}, roles(MyRole.ADMIN, MyRole.TEACHER))
        delete("/schedule:id", { ctx -> ScheduleController.delete(ctx, ctx.pathParam("id"))}, roles(MyRole.ADMIN, MyRole.TEACHER))

    }

    app.start()
}

fun getUserRole(ctx: Context): Role? {
    val curUser: User
    val login = ctx.basicAuthCredentials()!!.username
    val pass = ctx.basicAuthCredentials()!!.password

    val daoUser : Dao<User, Long> = DaoManager.createDao(DbUtils.source, User::class.java)

    val result = daoUser.queryForEq("login", login)

    if (result.isNotEmpty()){
        if (BCrypt.checkpw(pass, result.first().pass)) {
            curUser = result.first()
            return curUser.role
        }
    }

    return null
}