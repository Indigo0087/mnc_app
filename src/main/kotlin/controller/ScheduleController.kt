package controller

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import io.javalin.Context
import model.Schedule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import utils.Constants
import utils.DbUtils
import java.sql.SQLException

object ScheduleController {

    private lateinit var dao: Dao<Schedule, Long>
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    init {
        try {
            dao = DaoManager.createDao(DbUtils.source, Schedule::class.java)
        } catch (e : SQLException){
            logger.error("Err creating DAO " + e.message)
        }
    }

    fun create(ctx: Context) {
        val user = ctx.bodyAsClass(Schedule::class.java)
        try {
            if (timeAvailable(ctx)) {
                dao.create(user)
                ctx.status(Constants.CREATED_201)
                ctx.result("Success")
            } else
                ctx.status(Constants.BAD_REQUEST_400)
        } catch (e : SQLException){
            logger.error("Err saving")
            e.printStackTrace()
            ctx.status(Constants.INTERNAL_SERVER_ERROR_500)
        }
    }

    private fun toTime(time : String) : ArrayList<Long>{
        val m = time.split('-').toTypedArray()
        val array : ArrayList<Long> = arrayListOf()
        for (i in m.iterator()){
            val i1 = i.trim().split(':')[0]+i.trim().split(':')[1]
            array.add(i1.toLong())
        }
        return array
    }

    private fun timeAvailable(ctx: Context) : Boolean {
        var available = true
        val schedule = ctx.bodyAsClass(Schedule::class.java)
        val list = dao.queryForEq("class", schedule.classRoom).iterator()
        val curChekin = toTime(schedule.time!!)
        list.forEach {
            val chekin = toTime(schedule.time!!)
            val curTimeStart= curChekin[0]
            val curTimeEnd = curChekin[1]
            val start = chekin[0]
            val end = chekin[1]
            print("$curTimeStart >= $end && $curTimeEnd <= $start")
            if (curTimeStart < curTimeEnd) {
                if (it.weekDay!!.toString().toUpperCase() ==
                    schedule.weekDay.toString().toUpperCase()) {
                    if (curTimeStart >= end && curTimeEnd <= start) {//>= & <=
                        available = false
                        return available
                    }
                }
            } else {
                ctx.result("Time's incorrect")
                ctx.status(Constants.BAD_REQUEST_400)
                return false
            }
        }

        ctx.status(Constants.BAD_REQUEST_400)
        ctx.result("This time is not available!")
        return available
    }

    fun delete(ctx: Context, resourceId: String) {
        val userId = resourceId.toLong()
        try {
            dao.deleteById(userId)
        }catch (e : SQLException){
            logger.error("Err deleting")
            ctx.status(Constants.INTERNAL_SERVER_ERROR_500)
        }
    }

    fun getAll(ctx: Context) {
        try {
            ctx.json(dao.queryForAll())
        }catch (e : SQLException){
            logger.error("Err getting all")
            ctx.status(Constants.INTERNAL_SERVER_ERROR_500)
        }
    }

    fun getOne(ctx: Context, resourceId: String) {
        val userId = resourceId.toLong()
        try {
            val user = dao.queryForId(userId)
            if (user != null)
                ctx.json(user)
            else
                ctx.status(Constants.NOT_FOUND_404)
        } catch (e : SQLException){
            logger.error("Err getting one")
            ctx.status(Constants.INTERNAL_SERVER_ERROR_500)
        }
    }

    fun update(ctx: Context, resourceId: String) {
        val userId = resourceId.toLong()
        val user = ctx.bodyAsClass(Schedule::class.java)
        user.id = userId
        try {
            dao.update(user)
        }catch (e : SQLException){
            logger.error("bookId")
            ctx.status(Constants.INTERNAL_SERVER_ERROR_500)
        }
    }


}


