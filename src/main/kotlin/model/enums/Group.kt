package model.enums

enum class Group {
    STARTER, MIDDLE, ELDERS
}

enum class Rooms {
    R303, R306, R307, R308
}

enum class DaysOfWeek {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}