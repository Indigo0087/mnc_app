package model.enums

import io.javalin.security.Role

enum class MyRole : Role {
    STUDENT, TEACHER, ADMIN
}