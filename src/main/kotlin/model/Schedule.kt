package model

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import model.enums.DaysOfWeek
import model.enums.Group
import model.enums.Rooms

@DatabaseTable(tableName = "schedule")
data class Schedule (@DatabaseField(generatedId = true, columnName = "id")
                     var id : Long? = null,
                     @DatabaseField(columnName = "name")
                     var name: String? = null,
                     @DatabaseField(columnName = "class")
                     var classRoom : Rooms? = null,
                     @DatabaseField(columnName = "group")
                     var ageGroup : Group? = null,
                     @DatabaseField(columnName = "time")
                     var time: String? = null,
                     @DatabaseField(columnName = "day")
                     var weekDay: DaysOfWeek? = null) {
    constructor(id: Long?, name: String?, classRoom: Rooms?,
                ageGroup: Group?, time1: String?, time2: String, weekDay: DaysOfWeek?)
    :this(id, name, classRoom, ageGroup, "$time1-$time2", weekDay)
}