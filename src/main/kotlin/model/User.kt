package model

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import model.enums.MyRole

@DatabaseTable(tableName = "users")
data class User(@DatabaseField(generatedId = true)
                var id : Long? = null,
                @DatabaseField(columnName = "login")
                var login : String? = null,
                @DatabaseField(columnName = "role")
                var role : MyRole = MyRole.STUDENT,
                @DatabaseField(columnName = "password", canBeNull = false)
                var pass : String? = null)