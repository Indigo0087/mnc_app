package serialization

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import model.Schedule

class SchedSerial : StdSerializer<Schedule>(Schedule::class.java){

    override fun serialize(value: Schedule, gen: JsonGenerator, provider: SerializerProvider) {
        gen.writeStartObject()
        value.id?.let { gen.writeNumberField("id", it) }
        gen.writeStringField("name", value.name)
        gen.writeStringField("classRoom", value.classRoom.toString())
        gen.writeStringField("ageGroup", value.ageGroup.toString())
        gen.writeStringField("time", value.time.toString())
        gen.writeStringField("weekDay", value.weekDay.toString().toUpperCase())
        gen.writeEndObject()
    }

}